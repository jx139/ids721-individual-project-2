# Continuous Delivery of Rust Microservice
![pipeline status](https://gitlab.oit.duke.edu/jx139/ids721-individual-project-2/badges/main/pipeline.svg)

This project demonstrates a basic setup for a Rust microservice, encapsulating its deployment within a Docker container and automating the delivery process through a GitLab CI/CD pipeline. You can see the demo [video](https://drive.google.com/file/d/1yQK62H0bRRQtz1kpKP-yKfiegJkEhujf/view?usp=sharing) here.

## 1. Rust Microservice Functionality

The Rust microservice provides a simple REST API built with Actix-web, one of Rust's most popular web frameworks. The service includes a basic health check endpoint that responds to `GET` requests, confirming the service's operational status.

### Running Locally

To run the microservice locally, ensure you have Rust and Cargo installed. Then, execute:

```bash
cargo run
```

This will start the web server, and you can access the health check endpoint at `http://127.0.0.1:8080/health`.
![web](./pic/web.png)

## 2. Docker Configuration

The application is containerized using Docker, allowing for consistent deployment environments and easy version management.

### Building the Docker Image

To build the Docker image for the microservice, use the following command:

```bash
docker build -t rust_microservice .
```
![docker](./pic/docker.png)

This command should be run from the root directory of the project, where the `Dockerfile` is located.

### Running the Container

Once the image is built, you can run the container using:

```bash
docker run -p 8080:8080 rust_microservice
```
![run](./pic/run.png)

This will start the microservice inside a Docker container, mapping port 8080 of the container to port 8080 on your host, allowing you to access the health check endpoint as described above.

## 3. CI/CD Pipeline for Continuous Delivery

The project is equipped with a GitLab CI/CD pipeline for automating the build, test, and deployment processes.

### Pipeline Overview

The `.gitlab-ci.yml` file at the root of the project defines the pipeline configuration. The pipeline includes stages for testing, linting, formating, and building the Docker image and pushing it to a registry, from which it can be deployed.

### Key Stages

- **Build**: This stage builds the Docker image using the `Dockerfile` in the project.
- **Deploy**: This could be configured to deploy the Docker image to a live environment, such as a Kubernetes cluster or a VM.

### Running the Pipeline

The pipeline runs automatically on pushes to the repository. You can also trigger the pipeline manually through the GitLab UI.

### Required GitLab CI Variables

Ensure the following variables are set in your GitLab CI/CD settings:

- `CI_REGISTRY_USER`: Your GitLab registry username.
- `CI_JOB_TOKEN`: The job token for authentication.
- `CI_REGISTRY`: The URL of the GitLab Container Registry.

### Docker Image and Push

The pipeline logs into the GitLab Container Registry and pushes the built image, making it available for deployment.
